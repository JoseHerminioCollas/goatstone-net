# Goatstone Net

A general purpose server


### Run docker container

docker run -p 443:8443 gnet  NAME

### log into docker container
docker exec -it NAME /bin/sh

### set TEMPORARY cert
openssl req -newkey rsa:4096 -nodes -sha512 -x509 -days 365 -nodes -out sslcert/fullchain.pem -keyout sslcert/privkey.pem

### restart app
pm2 restart app

###
install with certbot

https://workaround.org/ispmail/jessie/create-certificate

### add to /etc/apt/sources.list

deb http://ftp.debian.org/debian jessie-backports main

### install certbot

apt-get install certbot -t jessie-backports

### install cert

certbot --webroot -w ./static -d <your_server_url>