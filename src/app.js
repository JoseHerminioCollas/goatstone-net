const express = require('express')
const https = require('https')
const fs = require('fs')

const app = express()

const basicAuth = require('express-basic-auth')
const coinRoute = require('./route/coin')
const coinValueRoute = require('./route/coin-value')
app.use(basicAuth(
    {
	users: { 'admin': 'admin' },
	challenge: true
    }))
let coinRouter = express.Router()
coinRouter.get('/', coinRoute)
const fromValue = 'ETH'
const toValues = ['USD', 'JPY']
let coinValueRouter = express.Router()
coinValueRouter.get('/', coinValueRoute(fromValue, toValues))
const options = {
    cert: fs.readFileSync('./sslcert/fullchain.pem'),
    key: fs.readFileSync('./sslcert/privkey.pem')
};
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    next()
})
app.use(express.static('static'));
app.use(function (req, res, next) {
    if(req.secure) {
	next()
    } else {
	res.redirect('https://' + req.headers.host + req.url)
    }
})
app.get('/health-check', (req, res) => {
    console.log('health check')
    res.sendStatus(200)
})
app.use('/coin', coinRouter)
app.use('/coin/value', coinValueRouter)

app.listen(8080, () => {
    console.log('app listening!')
})

https.createServer(options, app).listen(8443)

