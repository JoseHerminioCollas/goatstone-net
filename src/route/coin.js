const fetch = require('node-fetch');
// https://www.cryptocompare.com/media/19782/litecoin-logo.png
const coins = (req, res) => {
    const url =
        `https://min-api.cryptocompare.com/data/all/coinlist?extraParams=goatstone.hello`
    fetch(url)
        .then(res => res.json())
        .then(body => {
            res.status(200).send(body)
        })
}

module.exports = coins
