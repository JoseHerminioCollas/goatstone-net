const request = require('supertest')
const app = require('../src/app')

describe('root path', () => {
    test('it should respond to a GET method', () => {
        return request(app).get("/").then(response => {
            expect(response.statusCode).toBe(200)
        })
    })
})
describe('coin path', () => {
    test('it should respond to a GET method', () => {
        return request(app).get("/coin").then(response => {
            const b = response.body
            const c = Object.entries(b.Data).map(e => e[1])
            const d = c.map(e => e.Name)
            // console.log(':', c.slice(10, 11))
            expect(response.statusCode).toBe(200)
        })
    })
    test('it should respond to a GET method', () => {
        return request(app).get("/coin/value").then(response => {
            const b = response.body
            // console.log('coin value', b)
            expect(response.statusCode).toBe(200)
        })
    })
})
