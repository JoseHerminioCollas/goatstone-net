FROM node:8

RUN ["apt-get", "update"]
RUN ["apt-get", "install", "-y", "emacs"]

WORKDIR /app
COPY . /app
RUN npm install
RUN npm i -g pm2

EXPOSE 8080 8443

CMD ["pm2", "start", "src/app.js", "--no-daemon"]

